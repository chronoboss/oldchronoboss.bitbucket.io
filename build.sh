#!/bin/sh

mkdir "${ANDROID_HOME}/licenses" || true
echo "8933bad161af4178b1185d1a37fbf41ea5269c55" > "${ANDROID_HOME}/licenses/android-sdk-license"

sudo ./gradlew assembleDebug

# Magical file that makes bitbucket pipelines work. The echo'd string is not private nor personal. More info here: https://tiwiz.medium.com/building-android-with-bitbucket-pipelines-83cd62dbde33